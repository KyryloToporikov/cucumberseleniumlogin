package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public final class WebDriverFactory {


    public static WebDriver initDriver(){
        System.setProperty("webdriver.chrome.driver", Const.chromePath);
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Const.wait, TimeUnit.SECONDS);
        return driver;
    }
}
