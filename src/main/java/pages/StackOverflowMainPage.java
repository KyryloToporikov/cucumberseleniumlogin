package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class StackOverflowMainPage {

    private WebDriver driver;

    public StackOverflowMainPage(WebDriver driver){
        this.driver = driver;
    }

    @FindBy(xpath = "//a[contains(text(),'Log In')]")
    private WebElement logInBtn;

    @FindBy(id = "email")
    private WebElement emailField;

    @FindBy(id = "password")
    private WebElement passField;

    @FindBy(id = "submit-button")
    private WebElement submitBtn;

    @FindBy(xpath = "//a[@title='Recent inbox messages']")
    private WebElement inboxBtn;

    @FindBy(xpath = "//div[text()='The email or password is incorrect.']")
    private WebElement errorToolTip;

    public void openPage(){
        driver.navigate().to("https://stackoverflow.com/");
        driver.manage().window().maximize();
    }

    public void enterEmail(String email){
        emailField.click();
        emailField.sendKeys(email);
    }

    public void enterPass(String pass){
        passField.click();
        passField.sendKeys(pass);
    }

    public void clickLogInBtn(){
        logInBtn.click();
    }

    public void clickSubmitBtn(){
        submitBtn.click();
    }

    public boolean isInboxBtnDisplayed(){
        try{
            return inboxBtn.isDisplayed();
        }catch (NoSuchElementException e){
            return false;
        }
    }

    public boolean isErrorToolTipDisplayed(){
        try{
            return errorToolTip.isDisplayed();
        }catch (NoSuchElementException e){
            return false;
        }
    }

}
