Feature:
  As a user
  I want to be able to log in into StackOverflow account



  Scenario Outline: Log in into StackOverflow account
    Given user is opened StackOverflow home page
    When he clicks on log in button
    And he provides email as "<email>"
    And he provides password as "<password>"
    And he clicks on submit button
    Then he "<status>" logged in account
    Examples:
      | email           | password     | status         |
      | komicu_ky@bk.ru | Helloworld55 | successfully   |
      | komicu_ky@bk.ru | Helloworld66 | unsuccessfully |
