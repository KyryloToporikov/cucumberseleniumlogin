package steps;


import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(plugin = { "pretty" }, features = {"src/test/java/features"})
public class RunCukesTest extends AbstractTestNGCucumberTests{
}
