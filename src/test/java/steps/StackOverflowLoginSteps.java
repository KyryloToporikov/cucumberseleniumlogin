package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import pages.StackOverflowMainPage;
import utils.WebDriverFactory;

public class StackOverflowLoginSteps {


    private StackOverflowMainPage stackOverflowMainPage;
    private WebDriver driver;

    @Given("^user is opened StackOverflow home page$")
    public void user_is_opened_StackOverflow_home_page() throws Throwable {
        driver = WebDriverFactory.initDriver();
        stackOverflowMainPage = PageFactory.initElements(driver, StackOverflowMainPage.class);
        stackOverflowMainPage.openPage();
    }

    @When("^he clicks on log in button$")
    public void he_clicks_on_log_in_button() throws Throwable {
        stackOverflowMainPage.clickLogInBtn();
    }

    @And("^he provides email as \"([^\"]*)\"$")
    public void he_provides_email_as_tkyrylo_gmail_com(String email) throws Throwable {
        stackOverflowMainPage.enterEmail(email);
    }

    @And("^he provides password as \"([^\"]*)\"$")
    public void he_provides_password_as_zaqxsw(String password) throws Throwable {
        stackOverflowMainPage.enterPass(password);
    }

    @And("^he clicks on submit button$")
    public void he_clicks_on_submit_button() throws Throwable {
        stackOverflowMainPage.clickSubmitBtn();
    }

    @Then("^he \"([^\"]*)\" logged in account$")
    public void he_successfully_logged_in_account(String status) throws Throwable {
        if ("successfully".contains(status)) {
            Assert.assertTrue(stackOverflowMainPage.isInboxBtnDisplayed());
        }else if ("unsuccessfully".contains(status)){
            Assert.assertTrue(stackOverflowMainPage.isErrorToolTipDisplayed());
        }else {
            Assert.assertTrue(false, "Unexpected condition");
        }
    }
}
